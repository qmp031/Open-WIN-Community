---
layout: default
title: What to include in your repository
parent: Git and GitLab
has_children: false
nav_order: 2
---


# What to include in your repository
{: .fs-9 }

A guide to the information which should be included in your GitLab repository.
{: .fs-6 .fw-300 }

---

You can include whatever material you like in your repository. Below we have made some suggestions which may be helpful for future users who access your repository. Remember, the easier it is for people to reuse your material, the more likely they are to pick it up!

The suggestion below follows a style which has been developed to support sustainable open source software. It has been passed to Open WIN from Mozilla via the [Open Life Sciences](https://openlifesci.org) programme.

## Files to include in  your repository

Take a look at the Open WIN "[Data Sharing Decision Tree](https://git.fmrib.ox.ac.uk/open-science/community/data-sharing-decision-tree)" repository to see a completed collection of the below files. Note the files below are [named using CAPS](https://softwareengineering.stackexchange.com/questions/301691/readme-txt-vs-readme-txt) to make them easier to find!

### README.md

#### Sections to included

1. **The Problem**: Briefly describe what issue you are trying to address with this material
2. **The Solution**: Briefly describe how your material fixes the problem!
3. **What are we doing?**: Describe the activities of people involved in this material. What have you done so far and what are you intending to do next. This section could include **Usage instructions**, describing how users use the material in this repository.
4. **What do we need?**: Describe what contributions you would like to receive. Link to your CONTRIBUTING.md file for more information.
5. **Who are we?**: Identify who you are. Link to your lab pages.
6. **Contact us**: Give clear instructions for how people can get in touch.
7. **Acknowledgements and citation**: Give clear guidance on how people should cite your material. This should include the [doi for the repository](../repo-doi) and any supporting papers.


### CONTRIBUTING.md
This file describes what contributions you would like to this material and how people should get in touch. Remember, an important benefit of sharing your resource is to invite collaborators. Make it easy for people to get involved.

### CITATION.cff
This is a standardised file format which provides the exact metadata people will need to cite your repository (using the [doi you generate](../repo-doi)). It is easy to read by machines and humans, and is quickly becoming a sector standard - look out for a CITATION.cff in other software tools you use!

See [The Turing Way](https://the-turing-way.netlify.app/communication/citable/citable-cff.html) for information on what to include in a CITATION.cff file.

### LICENSE.md
This file lists the terms of reuse of your material. GitLab will suggest some standards. We recommend that you consider applying a [CC-BY-4.0](https://creativecommons.org/licenses/by/4.0/) licence to your material, so you will be properly attributed. For this license type, it is only necessary to include the line "Distributed under a CC-BY-4.0 license." for it to be legally binding. You may also wish to add some summary usage limitations and/or link to the full license wording.

Any material you produce while employed by the University of Oxford should be [copyright to the University](https://innovation.ox.ac.uk/university-members/commercialising-technology/ip-patents-licenses/software-copyright/)

See the [licensing guide](../repo-license) for a more complete discussion of which license you should consider applying to your code.
