---
layout: default
title: Benefits of Open Research
parent: Open WIN Community
has_children: false
nav_order: 3
---

# Benefits of Open Research
{: .fs-9 }

Why open research is good for you (and everyone else)
{: .fs-6 .fw-300 }

---

More information coming soon
{: .label .label-yellow }
