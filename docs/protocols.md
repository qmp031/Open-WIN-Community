---
layout: default
title: Open MR Protocols
has_children: true
nav_order: 2
---


# Open MR Protocols
{: .fs-9 }

How to share your magnetic resonance imaging protocols
{: .fs-6 .fw-300 }

---

![open-protocols](../../img/img-open-mrprot-flow.png)

## Purpose

The MR Protocols database is a repository for scanner protocols and associated information (for example radiographic procedures). Each entry in the database should contain sufficient information to enable to replication of data collection for a study, where appropriate hardware and licenses are available. Entries to the database are version controlled, so users can effectively map how and why changes were implmented.

The database is accessible via a web interface to enable frictionless sharing of materials both internally and externally.
Users are guided through the process of generating a digital object identifier (doi) and licence for their deposited material so they can receive appropriate attribution for reuse.

The database provides access to standard protocols and to the latest experimental protocols and where these have been uploaded by WIN members.

## Access the MR Protocols database
Available here: [http://open.win.ox.ac.uk/protocols/](http://open.win.ox.ac.uk/protocols/)

## How to use the MR Protocols database

Training guides are available for WIN members and external users. Please select the appropriate option from the buttons below, or the navigation menu.

[![For WIN members](../../img/btn-win.png)](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/protocols/protocols_guide_internal/)      [![For external researchers](../../img/btn-external.png)](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/protocols/protocols_guide_external/)


<!-- ## Citation

If you use this tool, please cite...

Coming soon
{: .label .label-yellow }
## License

This tool has been openly developed and shared using an open source license. You are free to...

Coming soon
{: .label .label-yellow } -->

## Working group members (alphabetically)
We are grateful to the following WIN members for their contributions to developing the Open MR Protocols database
- [Jon Campbell](https://www.win.ox.ac.uk/people/jon-campbell)
- [Stuart Clare](https://www.win.ox.ac.uk/people/stuart-clare)
- [Geoff Ferrari](https://www.linkedin.com/in/geoffrey-ferrari-a96871b2/?originalSubdomain=uk)
- [Dave Flitney](https://www.win.ox.ac.uk/people/david-flitney)
- [Gary Gibbs](https://www.linkedin.com/in/gary-gibbs-36a78541/?originalSubdomain=uk)
- [Clare Mackay](https://www.win.ox.ac.uk/people/clare-mackay)
- [Duncan Mortimer](https://www.win.ox.ac.uk/people/duncan-mortimer)
- [Sebastien Rieger](https://www.win.ox.ac.uk/people/sebastian-w-rieger)
