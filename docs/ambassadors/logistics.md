---
layout: default
title: Logistics
parent: Open WIN Ambassadors
grand_parent: Open WIN Community
has_children: false
nav_order: 3
---

# Logistics
{: .fs-9 }

Find out about our proposed logistics of running and participating in the Ambassadors program
{: .fs-6 .fw-300 }

---

Below we have answered some questions about how the pilot Ambassadors program will be structured. Jump straight to one of the questions or go through them all!

* [How many Ambassadors will take part in the pilot program?](#how-many-ambassadors-will-take-part-in-the-pilot-program)
* [What will the Ambassadors be working on?](#what-will-the-ambassadors-be-working-on)
* [How will the Ambassadors work together?](#how-will-the-ambassadors-work-together)
* [How long will Ambassadors be in-post for?](#how-long-will-ambassadors-be-in-post-for)
* [Who can be an ambassador?](#who-can-be-an-ambassador)
* [How can I become an Ambassador?](#wow-can-i-become-an-ambassador)


# How many Ambassadors will take part in the pilot program?

In this pilot program, we will look to engage up to ten Ambassadors. The number of Ambassadors may evolve in future iterations of the programme.

# What will the Ambassadors be working on?

In line with the aims of the Open Neuroimaging Project, the Ambassadors will work to increase the uptake of open science practices among WIN researchers. This will be achieved by creating and delivering training material for our infrastructure, and shaping the growth and direction of our community activities so that we can best meet the needs of WIN researchers. Ambassadors will be encouraged to pursue projects which interest them, and stretch, grow or capitalise on their existing skills as they choose.

# How will the Ambassadors work together?

While working on creating documentation, we envisage teams of two people working on each of the four focus areas of i) MR protocols, ii) tasks, iii) analysis, and iv) data. Others may work explicitly on community building and policy. In all these efforts, all Ambassadors will be invited to contribute to the material and interests of other teams, if they wish.

Ambassadors will be encouraged to work openly and publish their work-in-progress via GitLab. Training will be given to ensure all Ambassadors are comfortable and confident with this process.

Ambassadors will be encouraged to join 90 minute update and co-working calls, where we will work together on documentation and projects. Calls will take place on [Wonder](https://www.wonder.me/) and we will use [hackmd.io](http://hackmd.io/) to write collaborative notes during the call, using markdown so they can be saved directly to a git repository. We may use [padlet](https://padlet.com) for creative exercises. Take a look at the [programme schedule](../programme) to see how our calls will be structured.

# How long will Ambassadors be in-post for?

The pilot program will run from October 2021 to September 2022. All recruited Ambassadors will be expected to remain engaged with the program for this period, and should have confirmed posts as WIN members for the whole period.

# Who can be an Ambassador?

Anyone! Student, postdoc, core staff, professional services, PI...!

You do not need to have any knowledge or experience of open science. All we ask is that you are interested in contributing your time and energy to support the development of others. But this is not an entirely altruistic affair! By participating in this programme we expect you will directly [benefit](../benefits) in ways which are valuable to your career development and skills as a researcher. It should also be a fun and rewarding experience!

# How can I become an Ambassador?

A competitive application for this program was launched in September 2021. **The application process is now closed.** As part of the application you will be expected to describe why you are interested in becoming and Ambassador, what you will bring to the Ambassadors team and wider community, and what you will gain from being an Ambassador. You will also be asked to confirm that you have discussed the program with your Supervisor or Line Manager and they have approved your participation.

# What is expected of Ambassadors

Take a look at the [expectations](../expectations) of Ambassadors to learn more about the time commitments of participation.
