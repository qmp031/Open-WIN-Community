---
layout: default
title: Meet the Ambassadors
parent: Open WIN Ambassadors
grand_parent: Open WIN Community
has_children: false
nav_order: 1
---

# Meet the 2021-2022 Ambassadors
{: .fs-9 }

Find out about the current Open WIN Ambassadors
{: .fs-6 .fw-300 }

---

More information coming soon
{: .label .label-yellow }




### Verena Sarrazin




### Yingshi Feng





### Dejan Draschkow




### Bernd Taschler

Bernd is a postdoc in WIN's analysis group. See his profile [here](https://www.win.ox.ac.uk/people/bernd-taschler).
