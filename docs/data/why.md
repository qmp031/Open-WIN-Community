---
layout: default
title: Why should I share my data?
parent: Open Data
has_children: false
nav_order: 1
---


# Why should I share my data?
{: .fs-9 }

Reasons why would should consider sharing your data
{: .fs-6 .fw-300 }

---

## Why should I share my data?

<details>
 <summary><b>To create a citable research output</b></summary><br>

 Collecting and curating your research data is often a huge undertaking which requires significant skill and expertise. You deserve to receive credit for the data you have created as an independent research object, over and above any publish manuscript. We will help you to share your data in a way that it can exist as a citable research object, on your CV and the digital realm.
<br><br></details>


<details>
 <summary><b>Data sharing is required by some publishers</b></summary><br>

 Many publishers will now require you to share data, so your research findings can be verified. Our infrastructure will enable you to fulfil these requirements. Caution: Don't wait until you are about to publish before thinking about data sharing! It is much easier to share data if you have built in sharing as part of your study.
<br><br></details>


<details>
 <summary><b>Funders want to make the most of their investment</b></summary><br>

  The [OECD Principles and Guidelines for Access to Research Data from Public Funding](https://www.oecd.org/sti/sci-tech/38500813.pdf) (2007) promotes a culture of openness and sharing to increase “the return on public investments in scientific research.”
<br><br></details>


<details>
 <summary><b>Sharing is becoming the norm</b></summary><br>

 With funders and publishers increasingly prioritising data sharing, it is now common to see some level of shared data accompanying a publication. [Fears of being "scooped" may be unfounded](https://quantpalaeo.wordpress.com/2018/02/26/been-scooped-a-discussion-on-data-stewardship/) and appropriately managed access can ensure that data are released with a timeframe you are comfortable with.
<br><br></details>

<details>
 <summary><b>You can choose what to share</b></summary><br>

 You can choose to share raw or processed data according to your requirements, preferences, ethics and available infrastructure. Even if you are only able to share the table of derived statistics which are used to generate your figures, that may be a significant step-change of activity (requiring a reasonable outlay of effort) and should not be minimised as an effort towards transparency.
<br><br></details>



<details>
 <summary><b>To create a sustainable archive of your data</b></summary><br>

 WIN has a number of systems to ensure your data are backed up in some form or another, at various stages of the research process. This tool is designed to complement these systems as a public-facing and accessible archive. <mark>Add comment about sustainability of platform"</mark>
<br><br></details>



<details>
 <summary><b>To speed up translation</b></summary><br>

 Sharing your data means it is easier for other researchers to replicate and extend your work, which may ultimately speed up translation. For example, another research team may conduct exploratory research using your data, and then test the hypothesis on a newly collected dataset. This may half the time taken to reach the same conclusion.
<br><br></details>
